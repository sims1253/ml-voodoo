Well hello there

You should use Python 3!
I'd advise for the use of something like [pipenv](https://docs.pipenv.org/)
(or just virtualenv and pip seperate)

There is a `Pipfile` and `Pipfile.lock` in the project that contain all the
needed libraries, Pipfile.lock has working versions included, that you can use
to install all packages from with `Pipenv install` for the `Pipfile` or
`pipenv install --ignore-pipfile` for the `Pipfile.lock`.
There is also a `requirements.txt` file to use via
`pip install -r requirements.txt`. This does not pin versions though.
Finally, I added ipython and jupyter as they might come in handy for quick
console prototyping or handling notebooks we are given as well as coala as I
like to run it on all my code.

For more smooth python coding, a proper IDE as eg.
[PyCharm](https://www.jetbrains.com/pycharm/download/#section=linux) can be a
great help.