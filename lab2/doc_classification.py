
from __future__ import print_function, division
from io import open

import time

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import Normalizer
from sklearn.pipeline import make_pipeline
from sklearn.feature_selection import SelectKBest
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split

from lecture2 import Perceptron
from pegasos import SVC
from pegasos import LogisticRegression


# This function reads the corpus, returns a list of documents, and a list
# of their corresponding polarity labels.
def read_data(corpus_file):
    X = []
    Y = []
    with open(corpus_file, encoding='utf-8') as f:
        for line in f:
            words = line.strip().split()
            X.append(words[3:])
            Y.append(words[1])
    return X, Y


if __name__ == '__main__':

    # Read all the documents.
    X, Y = read_data('data/all_sentiment_shuffled.txt')

    # Split into training and test parts.
    Xtrain, Xtest, Ytrain, Ytest = train_test_split(X, Y, test_size=0.2,
                                                    random_state=0)

    # Set up the preprocessing steps and the classifier.

    classifiers = [['Perceptron', Perceptron],
                   ['SVC', SVC],
                   ['Logistic Regression', LogisticRegression]]

    for name, cls in classifiers:

        pipeline = make_pipeline(
            TfidfVectorizer(preprocessor=lambda x: x,
                            tokenizer=lambda x: x),
            SelectKBest(k=1000),
            Normalizer(),
            cls()
        )

        print(name)
        # Train the classifier.
        t0 = time.time()
        pipeline.fit(Xtrain, Ytrain)
        t1 = time.time()
        print('Training time: {:.2f} sec.'.format(t1-t0))

        # Evaluate on the test set.
        Yguess = pipeline.predict(Xtest)
        print('Accuracy: {:.4f}.\n'.format(accuracy_score(Ytest, Yguess)))
