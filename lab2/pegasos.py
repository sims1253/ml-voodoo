import numpy as np
from random import randint
from lecture2 import Perceptron
from math import exp


class SVC(Perceptron):
    # Simply overloading the fit function with the pegasos hinge loss version
    def fit(self, X, Y):
        """
        Train a linear classifier using the pegasos learning algorithm.
        """

        # First determine which output class will be associated with positive
        # and negative scores, respectively.
        self.find_classes(Y)

        # Convert all outputs to +1 (for the positive class) or -1 (negative).
        Ye = self.encode_outputs(Y)

        # If necessary, convert the sparse matrix returned by a vectorizer
        # into a normal NumPy matrix.
        if not isinstance(X, np.ndarray):
            X = X.toarray()

        # Initialize the weight vector to all zeros.
        n_features = X.shape[1]
        self.w = np.zeros(n_features)

        # Pegasos algorithm using hinge loss:
        self.w = pegasos(X, Ye, 1 / X.shape[0], hinge_loss,
                         10 * X.shape[0], True)


class LogisticRegression(Perceptron):
    # Simply overloading the fit function with the pegasos log loss version
    def fit(self, X, Y):
        """
        Train a linear classifier using the pegasos learning algorithm.
        """

        # First determine which output class will be associated with positive
        # and negative scores, respectively.
        self.find_classes(Y)

        # Convert all outputs to +1 (for the positive class) or -1 (negative).
        Ye = self.encode_outputs(Y)

        # If necessary, convert the sparse matrix returned by a vectorizer
        # into a normal NumPy matrix.
        if not isinstance(X, np.ndarray):
            X = X.toarray()

        # Initialize the weight vector to all zeros.
        n_features = X.shape[1]
        self.w = np.zeros(n_features)

        # Pegasos algorithm using log loss:
        self.w = pegasos(X, Ye, 1 / X.shape[0], log_loss,
                         10 * X.shape[0], True)


def pegasos(feature_vectors: np.ndarray,
            outputs: np.ndarray,
            reg_par: float,
            weight_updater,
            iterations=100,
            random=True):
    """
    Implements the pegasos algorithm as shown in
    http://www.cse.chalmers.se/~richajo/dit865/files/a2_clarification.pdf
    Allows for different weight update functions

    :param feature_vectors: a list of example feature vectors X
    :param outputs: a list of outputs Y
    :param reg_par: regularization parameter λ
    :param weight_updater: the function used to update the weight vector
    :param iterations: The numbers of iterations the algorithm should run.
    :param random: True if training feature vectors should be chosen randomly
    :return: weight vector
    """
    weights = np.ones(feature_vectors.shape[1])

    if random:
        for i in range(iterations):
            # Get a random number and get the matching x,y training pair
            random_index = randint(0, feature_vectors.shape[0] - 1)
            x_instance = feature_vectors[random_index]
            y_instance = outputs[random_index]
            # Update the weights using the chosen training pair
            weights = weight_updater(reg_par, i, weights, x_instance,
                                     y_instance)
    else:
        for i in range(iterations):
            # Get the x,y training pair that matches the current index
            x_instance = feature_vectors[i % feature_vectors.shape[0]]
            y_instance = outputs[i % feature_vectors.shape[0]]
            # Update the weights using the chosen training pair
            weights = weight_updater(reg_par, i, weights, x_instance,
                                     y_instance)

    return weights


def hinge_loss(reg_par: float,
               i: int,
               weights: np.ndarray,
               x_instance: np.ndarray,
               y_instance: np.int64):
    """
    Weight update function for the pegasos algorighm using hinge loss

    :param reg_par: regularization parameter λ from pegasos
    :param i: iteration index from pegasos
    :param weights: weight vector
    :param x_instance: current training feature vector
    :param y_instance: current output for x_instance
    :return: the changed weight vector
    """

    # This is essentially a copy paste of the pseudo code
    etha = 1 / (reg_par * (i + 1))
    score = weights.dot(x_instance)

    if y_instance * score < 1:
        return (1 - etha * reg_par) * weights + (etha * y_instance * x_instance)
    else:
        return (1 - etha * reg_par) * weights


def log_loss(reg_par: float,
             i: int,
             weights: np.ndarray,
             x_instance: np.ndarray,
             y_instance: np.int64):
    """
    Weight update function for the pegasos algorighm using log loss

    :param reg_par: regularization parameter λ from pegasos
    :param i: iteration index from pegasos
    :param weights: weight vector
    :param x_instance: current training feature vector
    :param y_instance: current output for x_instance
    :return: the changed weight vector
    """
    step_length = 1 / (reg_par * (i + 1))
    score = weights.dot(x_instance)

    # This is needed as the exp() function throws math range errors
    # nondeterministically.
    # As this leads to the log loss being very small, it's the denominator
    # that's being "too big", we set log_loss to 0 in those cases, as an
    # approximation that doesn't crash and should not change the result a lot.
    # We essentially do: if denominator > exp(~300): log loss = 0 which should
    # lead to an error somewhere below the machine precision
    try:
        nominator = -1 * y_instance
        denominator = 1 + exp(y_instance * weights.dot(x_instance))
        log_loss = (nominator/denominator) * x_instance
    except:
        # This should actually be a vector of 0s but that wouldn't change
        # anything so this is probably slightly cheaper
        log_loss = 0

    # With each iteration, we downscale the weights vector a little less
    rescaled_weights = (1 - step_length * reg_par) * weights

    if y_instance * score < 1:
        return rescaled_weights - (step_length * log_loss)
    else:
        return rescaled_weights
