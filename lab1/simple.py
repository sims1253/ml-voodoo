import numpy as np
import re
import pandas as pd
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import make_pipeline
from sklearn.dummy import DummyClassifier
from sklearn.model_selection import cross_validate
from sklearn.linear_model import Perceptron
from sklearn.metrics import accuracy_score
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import SelectPercentile

tokenize_re = re.compile(r'''
                         \d+[:\.]\d+
                         |(https?://)?(\w+\.)(\w{2,})+([\w/]+)
                         |[@\#]?\w+(?:[-']\w+)*
                         |[^a-zA-Z0-9 ]+''',
                         re.VERBOSE
                         )


def tokenize(text):
    return [m.group() for m in tokenize_re.finditer(text)]


dev_tweet_list = []
dev_y_list = []

with open('./lab1/data/dev.tsv') as file:
    for line in file.readlines():
        tweet_arr = tokenize(line)

        dev_tweet_list.append(tweet_arr[1:])
        dev_y_list.append(tweet_arr[0])

with open('./lab1/data/train.tsv') as file:
    train_tweet_list = []
    train_y_list = []

    for line in file.readlines():
        tweet_arr = tokenize(line)

        train_tweet_list.append(tweet_arr[1:])
        train_y_list.append(tweet_arr[0])

    vectorizer = CountVectorizer(
        preprocessor=lambda x: x, tokenizer=lambda x: x)
    classifier = Perceptron(max_iter=2000, shuffle=True)

    counts = vectorizer.fit_transform(train_tweet_list)
    classifier.fit(counts, train_y_list)

    dev_counts = vectorizer.transform(dev_tweet_list)

    y_guess = classifier.predict(dev_counts)
    res = accuracy_score(dev_y_list, y_guess)

    print(res)
