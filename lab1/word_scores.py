import numpy as np
import pandas as pd
from sklearn.model_selection import RepeatedStratifiedKFold
import re
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import make_pipeline
from sklearn.linear_model import Perceptron
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from collections import defaultdict


tokenize_re = re.compile(r'''
                         \d+[:\.]\d+
                         |(https?://)?(\w+\.)(\w{2,})+([\w/]+)
                         |[@\#]?\w+(?:[-']\w+)*
                         |[^a-zA-Z0-9 ]+''',
                         re.VERBOSE
                         )


def tokenize(text):
    return [m.group() for m in tokenize_re.finditer(text)]


# Preparation of the combined dataset from dev and training data
dev_tweet_list = []
dev_y_list = []
with open('./data/dev.tsv') as file:
    for line in file.readlines():
        tweet_arr = tokenize(line)
        dev_tweet_list.append(tweet_arr[2:])
        dev_y_list.append(tweet_arr[0])

with open('./data/train.tsv') as file:
    for line in file.readlines():
        tweet_arr = tokenize(line)
        dev_tweet_list.append(tweet_arr[2:])
        dev_y_list.append(tweet_arr[0])

# Build the self trained word score dict by collecting all individual word
# scores and calculating the average for each word
internal_word_score_dict = defaultdict(list)
for index, tweet in enumerate(dev_tweet_list):
    for word in tweet:
        if dev_y_list[index] == 'positive':
            internal_word_score_dict[word].append(1)
        elif dev_y_list[index] == 'negative':
            internal_word_score_dict[word].append(-1)
        else:
            internal_word_score_dict[word].append(0)

for key in internal_word_score_dict.keys():
    internal_word_score_dict[key] = sum(internal_word_score_dict[key]) / len(
        internal_word_score_dict[key])

# Build the external word dict from the provided file
external_word_score_dict = {}
with open('./data/semeval.txt') as file:
    for line in file.readlines():
        splits = line.split()
        external_word_score_dict[str.join('', splits[1:])] = float(splits[0])

# Preparation of the validator
rskf = RepeatedStratifiedKFold(n_splits=5, n_repeats=5, random_state=42)

tweet_array = np.array(dev_tweet_list)
y_array = np.array(dev_y_list)
external_results = []
internal_results = []
# For each of the 80/20 splits predict for the test set and append the accuracy
# to the result list
for train_index, test_index in rskf.split(tweet_array, y_array):
    X_train, X_test = tweet_array[train_index], tweet_array[test_index]
    y_train, y_test = y_array[train_index], y_array[test_index]

    external_tweet_scores = []
    internal_tweet_scores = []
    # For all tweets, collect the word scores and sum them up to get a tweet
    # score
    for tweet in X_test:
        external_scores = []
        internal_scores = []
        for word in tweet:
            try:
                external_scores.append(external_word_score_dict[word])
                internal_scores.append(internal_word_score_dict[word])
            except:
                external_scores.append(0)
                internal_scores.append(0)
        external_tweet_scores.append(sum(external_scores))
        internal_tweet_scores.append(sum(internal_scores))

    # Normalize the external tweet scores to 1 or -1 depending on the highest
    # absolute score
    max_s = max(external_tweet_scores)
    min_s = min(external_tweet_scores)
    if max_s < min_s * -1:
        norm_scores = [score / min_s * -1 for score in external_tweet_scores]
    else:
        norm_scores = [score / max_s for score in external_tweet_scores]

    df = pd.DataFrame(norm_scores)
    mean = df[0].mean()
    sd = df[0].std()

    # Classify tweets depending on their score in relation to the mean of all
    # tweet scores
    y_guess = []
    for score in norm_scores:
        if score > mean + (0.3 * sd):
            y_guess.append('positive')
        elif score < mean - (1.5 * sd):
            y_guess.append('negative')
        else:
            y_guess.append('neutral')

    external_results.append(accuracy_score(y_test, y_guess))

    # Normalize the internal tweet scores to 1 or -1 depending on the highest
    # absolute score
    max_s = max(internal_tweet_scores)
    min_s = min(internal_tweet_scores)
    if max_s < min_s * -1:
        norm_scores = [score / min_s * -1 for score in internal_tweet_scores]
    else:
        norm_scores = [score / max_s for score in internal_tweet_scores]

    df = pd.DataFrame(norm_scores)
    mean = df[0].mean()
    sd = df[0].std()

    # Classify tweets depending on their score in relation to the mean of all
    # tweet scores
    y_guess = []
    for score in norm_scores:
        if score > mean + (0.3 * sd):
            y_guess.append('positive')
        elif score < mean - (1.1 * sd):
            y_guess.append('negative')
        else:
            y_guess.append('neutral')

    internal_results.append(accuracy_score(y_test, y_guess))

# Calculate and print the confusion matrix for the last guess with the internal
# dictionary
cm = confusion_matrix(y_test, y_guess,
                      labels=["positive", "neutral", "negative"])
cm_n = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
print(cm_n)

# Calculate the descriptive statistics for the external and internal results
df_e = pd.DataFrame(external_results)
df_i = pd.DataFrame(internal_results)
print(df_e.describe()[0])
print(df_i.describe()[0])
