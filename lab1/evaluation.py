import numpy as np
import pandas as pd
from sklearn.model_selection import RepeatedStratifiedKFold
import re
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import make_pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import Perceptron
from sklearn.svm import LinearSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.dummy import DummyClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix


tokenize_re = re.compile(r'''
                         \d+[:\.]\d+
                         |(https?://)?(\w+\.)(\w{2,})+([\w/]+)
                         |[@\#]?\w+(?:[-']\w+)*
                         |[^a-zA-Z0-9 ]+''',
                         re.VERBOSE
                         )


def tokenize(text):
    return [m.group() for m in tokenize_re.finditer(text)]


# Preparation of the combined dataset from dev and training data
dev_tweet_list = []
dev_y_list = []
with open('./data/dev.tsv') as file:
    for line in file.readlines():
        tweet_arr = tokenize(line)
        dev_tweet_list.append(tweet_arr[2:])
        dev_y_list.append(tweet_arr[0])

with open('./data/train.tsv') as file:
    for line in file.readlines():
        tweet_arr = tokenize(line)
        dev_tweet_list.append(tweet_arr[2:])
        dev_y_list.append(tweet_arr[0])

# Preparation of the validation and pipeline parts
rskf = RepeatedStratifiedKFold(n_splits=5, n_repeats=5, random_state=42)
cv = CountVectorizer(preprocessor=lambda x: x, tokenizer=lambda x: x)
classifiers = [['LogistigRegression', LogisticRegression()],
               ['Perceptron', Perceptron(max_iter=10, shuffle=True)],
               ['DecisionTree', DecisionTreeClassifier(max_depth=100)],
               ['LinearSVC', LinearSVC(random_state=42)],
               ['Dummy', DummyClassifier()]]

# Run validation cycle for each classifier
for name, classifier in classifiers:
    pipeline = make_pipeline(cv, classifier)

    tweet_array = np.array(dev_tweet_list)
    y_array = np.array(dev_y_list)
    results = []
    # For each of the 80/20 splits fit the classifier, predict for the test
    # set and append the accuracy to the result list
    for train_index, test_index in rskf.split(tweet_array, y_array):
        X_train, X_test = tweet_array[train_index], tweet_array[test_index]
        y_train, y_test = y_array[train_index], y_array[test_index]

        pipeline.fit(X_train, y_train)
        y_guess = pipeline.predict(X_test)
        results.append(accuracy_score(y_test, y_guess))

    # Calculate the confusion matrix for the last result and normalize it to 1
    cm = confusion_matrix(y_test, y_guess,
                          labels=["positive", "neutral", "negative"])
    cm_n = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    # Prepare data frame for descriptive statistics
    df = pd.DataFrame(results)

    print("####################################################################"
          "############")
    print(name, '\n')
    print('Confusion Matrix:')
    print(cm_n, '\n')
    print('Descriptive Statistics')
    print(df.describe()[0], '\n')
